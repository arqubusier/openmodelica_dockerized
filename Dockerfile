FROM debian:jessie
MAINTAINER alimguzhin@di.uniroma1.it

COPY *.sh /build/

RUN /build/prepare.sh && /build/openmodelica.sh && /build/cleanup.sh && rm -rf /build

RUN useradd -ms /bin/bash openmodelica

USER openmodelica
ENV USER openmodelica

CMD ["OMShell-terminal"]
